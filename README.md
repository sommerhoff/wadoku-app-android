WaDoku-App für Android

WaDoku ist ein Japanisch-Deutsch Online-Wörterbuch.

Programmierer: Paul C. Sommerhoff

Kontakt WaDoku-Projekt: Dr. Ulrich Apel

Quellcode:
https://bitbucket.org/sommerhoff/wadoku-app-android


Lizenz Quellcode: Paul C. Sommerhoff, CC BY-SA (https://creativecommons.org/licenses)

Lizenz WaDoku-Daten: Ulrich Apel, CC BY-SA und in Teilen WaDoku e.V. (http://www.wadoku.de/wiki/display/WAD/Wadoku.de-Daten+Lizenz)