package de.miraisoft.wadoku;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.StrictMode;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URLEncoder;
import java.util.HashSet;
import java.util.Set;

/**
 * WaDoku app for Android
 * @author Paul C. Sommerhoff
 */

public class MainActivity extends Activity {

    public static final String PREF_ID = "de.miraisoft.wadoku";
    public static final String PREF_USE_FAVORITES = "use_favorites";
    public static final int FURIGANA_SIZE = 12;
    public static final int KANJI_SIZE = 16;
    private static final int LIMIT_DEFAULT = 30;

    JSONObject json;
    LinearLayout layout;
    SharedPreferences settings;
    boolean useFavorites = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                int id = item.getItemId();

                if (id == R.id.action_info) {
                    Intent intent = new Intent(MainActivity.this, InfoActivity.class);
                    startActivity(intent);
                }

                if (id == R.id.action_settings) {
                    Intent intent = new Intent(MainActivity.this, SettingsActivity.class);
                    startActivity(intent);
                }

                if (id == R.id.action_favorites) {
                    Intent intent = new Intent(MainActivity.this, FavoritesActivity.class);
                    startActivity(intent);
                }

                if (id == R.id.action_exit) {
                    System.exit(0);
                }
                return false;
            }
        });

        // init
        EditText txtInput = findViewById(R.id.input);
        txtInput.setHint(R.string.search_expression);
        int mode =  getResources().getConfiguration().uiMode &
                Configuration.UI_MODE_NIGHT_MASK;
        if(mode == Configuration.UI_MODE_NIGHT_YES) {
            txtInput.setBackgroundColor(ContextCompat.getColor(this, R.color.blackish));
            txtInput.setHintTextColor(ContextCompat.getColor(this, R.color.grey));
        }

        ScrollView scrollView = (ScrollView)findViewById(R.id.scrollView);
        layout = new LinearLayout(this);
        layout.setOrientation(LinearLayout.VERTICAL);
        scrollView.addView(layout);

        settings = getApplicationContext().getSharedPreferences(PREF_ID, MODE_PRIVATE);

        // only for testing, comment in productive mode
        //settings.edit().clear().commit();

        if(settings.getInt("version", 0) < 6) {
            int dialogTheme = -1;
            if(mode == Configuration.UI_MODE_NIGHT_YES) {
                dialogTheme = R.style.UpdateAlertDialogDark;
            }
            else {
                dialogTheme = R.style.UpdateAlertDialog;
            }
            AlertDialog.Builder builder = new AlertDialog.Builder(this, dialogTheme);
            builder.setTitle(R.string.title_update_v3_0)
                    .setMessage(R.string.message_update_v3_0)
                    .setNeutralButton("OK", null)
                    .show();

            SharedPreferences.Editor editor = settings.edit();
            editor.putInt("version", 6);
            editor.putBoolean(PREF_USE_FAVORITES, true);
            editor.apply();
        }
    }

    /** start search */
    public void doSearch(View v) {
        useFavorites = settings.getBoolean("use_favorites", false);
        try {
            if(!isNetworkAvailable()) {
                Toast.makeText(getApplicationContext(), R.string.error_internet, Toast.LENGTH_LONG).show();
                return;
            }
            EditText input = (EditText)findViewById(R.id.input);
            final String search = URLEncoder.encode(input.getText().toString(), "UTF-8");
            // no offset for first search
            getWadokuData(search, 0);

        } catch (Exception e) {
            Toast.makeText(getApplicationContext(), R.string.error_text, Toast.LENGTH_LONG).show();
        }
    }

    /** retrieve WaDoku data from wadoku.eu */
    private void getWadokuData(final String search, final int offset) {
        try {
            String url = "https://wadoku.eu:10010/api/v1/search?query=" + search
                    + "&offset=" + offset + "&format=html"
                    + "&limit=" + LIMIT_DEFAULT;
            json = JsonReader.readJsonFromUrl(url);

            layout.removeAllViews();

            final int total = json.getInt("total");
            final int limit;
            if(offset + json.getInt("limit") > total) {
                limit = total % LIMIT_DEFAULT;
            } else {
                limit = json.getInt("limit");
            }

            if(total == 0) {
                Toast.makeText(getApplicationContext(), R.string.no_results, Toast.LENGTH_LONG).show();
                return;
            }

            for(int i=0; i < limit; i++) {
                if(json.getJSONArray("entries").length() == i) {
                    // problem with WaDoku API ("total" value)favs
                    break;
                }
                JSONObject jsonEntry = json.getJSONArray("entries").getJSONObject(i);

                final TextView tvFurigana = new TextView(this);
                final String furigana = jsonEntry.getString("furigana");
                tvFurigana.setText(furigana);
                tvFurigana.setTextSize(FURIGANA_SIZE);
                layout.addView(tvFurigana);

                TextView tvKanji = new TextView(this);
                final String kanji = removeLongKanjiTag(jsonEntry.getString("writing"));
                tvKanji.setText(kanji);
                tvKanji.setTextSize(KANJI_SIZE);
                tvKanji.setTypeface(tvKanji.getTypeface(), Typeface.BOLD);
                layout.addView(tvKanji);

                WebView wvGerman = new WebView(this);
                String css = readRawTextFile(this, R.raw.style);
                String htmlData = "<html><head><style type=\"text/css\">" + css + "</style></head>"
                        + "<body>" + correctHref(jsonEntry.getString("definition")) + "</body></html>";
                wvGerman.loadDataWithBaseURL("", htmlData, "text/html", "UTF-8", null);
                final String german = htmlData;
                layout.addView(wvGerman);

                if(jsonEntry.has("sub_entries")) {
                    //JSONObject subEntries = jsonEntry.getJSONObject("sub_entries").getJSONObject("entries");
                    // TODO API: fullsubentries does not work
                }

                final String wadokuId = jsonEntry.getString("wadoku_id");
                Button btnFavorite = null;
                if(useFavorites) {
                    if (!settings.contains("furigana_" + wadokuId)) {
                        btnFavorite = new Button(this);
                        btnFavorite.setTextColor(ContextCompat.getColor(this, R.color.favButtonText));
                        btnFavorite.setText("★ zu Favoriten hinzufügen");
                        btnFavorite.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                SharedPreferences.Editor editor = settings.edit();
                                editor.putString("furigana_" + wadokuId, furigana);
                                editor.putString("kanji_" + wadokuId, kanji);
                                editor.putString("german_html_" + wadokuId, german);

                                Set<String> favs = settings.getStringSet("favorites", new HashSet<String>());
                                if (!favs.contains(wadokuId)) {
                                    favs.add(wadokuId);
                                }
                                editor.putStringSet("favorites", favs);
                                editor.apply();

                                layout.removeView(v);
                                Toast.makeText(getApplicationContext(), "Eintrag zu Favoriten hinzugefügt.", Toast.LENGTH_LONG).show();
                            }
                        });
                        layout.addView(btnFavorite);
                    }
                }

                if(i % 2 == 0) {
                    tvFurigana.setBackgroundResource(R.color.bg_entry1);
                    tvKanji.setBackgroundResource(R.color.bg_entry1);
                    wvGerman.setBackgroundColor(getResources().getColor(R.color.bg_entry1));
                    if(btnFavorite != null) {
                        btnFavorite.setBackgroundResource(R.color.bg_entry1);
                    }
                } else {
                    tvFurigana.setBackgroundResource(R.color.bg_entry2);
                    tvKanji.setBackgroundResource(R.color.bg_entry2);
                    wvGerman.setBackgroundColor(getResources().getColor(R.color.bg_entry2));
                    if(btnFavorite != null) {
                        btnFavorite.setBackgroundResource(R.color.bg_entry2);
                    }
                }
            }

            if(offset + limit < total) {
                Button btNext = new Button(this);
                btNext.setText((offset + limit) + "/" + total + "   " + getResources().getString(R.string.btnext_more));
                btNext.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        getWadokuData(search, offset + limit);
                    }
                });
                layout.addView(btNext);
            }

        } catch (Exception e) {
            System.err.println(e);
            Toast.makeText(getApplicationContext(), R.string.error_text, Toast.LENGTH_LONG).show();
        }
    }

    private String removeLongKanjiTag(String kanji) {
        return kanji.replaceAll("[(<].*[>)]", "");
    }

    private String correctHref(String html) {
        return html.replaceAll("href=\"", "href=\"http://wadoku.eu")
                .replaceAll("href='", "href='http://wadoku.eu");
    }

    /* by robd (at stackoverflow.com) */
    public static String readRawTextFile(Context ctx, int resId)
    {
        InputStream inputStream = ctx.getResources().openRawResource(resId);

        InputStreamReader inputreader = new InputStreamReader(inputStream);
        BufferedReader buffreader = new BufferedReader(inputreader);
        String line;
        StringBuilder text = new StringBuilder();

        try {
            while (( line = buffreader.readLine()) != null) {
                text.append(line);
                text.append('\n');
            }
        } catch (IOException e) {
            return null;
        }
        return text.toString();
    }

    /* by Alexandre Jasmin (at stackoverflow.com) */
    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }
}
