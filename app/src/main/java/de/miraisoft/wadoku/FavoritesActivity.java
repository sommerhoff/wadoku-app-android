package de.miraisoft.wadoku;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class FavoritesActivity extends Activity {

    SharedPreferences settings;
    LinearLayout layout;
    Button btnRemove;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_favorites);

        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                int id = item.getItemId();

                if (id == R.id.action_settings) {
                    Intent intent = new Intent(FavoritesActivity.this, SettingsActivity.class);
                    startActivity(intent);
                }
                return false;
            }
        });

        ScrollView scrollView = findViewById(R.id.scvFavorites);
        layout = new LinearLayout(this);
        layout.setOrientation(LinearLayout.VERTICAL);
        scrollView.addView(layout);

        settings = getApplicationContext().getSharedPreferences(MainActivity.PREF_ID, MODE_PRIVATE);
        Set<String> favs = settings.getStringSet("favorites", new HashSet<String>());
        int i = 0;
        for(String id: favs) {

            final TextView tvFurigana = new TextView(this);
            final String furigana = settings.getString("furigana_" + id, "");
            tvFurigana.setText(furigana);
            tvFurigana.setTextSize(MainActivity.FURIGANA_SIZE);
            layout.addView(tvFurigana);

            final TextView tvKanji = new TextView(this);
            final String kanji = settings.getString("kanji_" + id, "");
            tvKanji.setText(kanji);
            tvKanji.setTextSize(MainActivity.KANJI_SIZE);
            tvKanji.setTypeface(tvKanji.getTypeface(), Typeface.BOLD);
            layout.addView(tvKanji);

            final WebView wvGerman = new WebView(this);
            String css = MainActivity.readRawTextFile(this, R.raw.style);
            String htmlData = settings.getString("german_html_" + id, "");
            wvGerman.loadDataWithBaseURL("", htmlData, "text/html", "UTF-8", null);
            layout.addView(wvGerman);

            final String wadokuId = id;
            btnRemove = null;
            if(!settings.contains("entry_" + wadokuId)) {
                btnRemove = new Button(this);
                btnRemove.setText("entfernen");
                btnRemove.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        SharedPreferences.Editor editor = settings.edit();
                        editor.remove("furigana_" + wadokuId);
                        editor.remove("kanji_" + wadokuId);
                        editor.remove("german_html_" + wadokuId);

                        Set<String> favs = settings.getStringSet("favorites", new HashSet<String>());
                        if (favs.contains(wadokuId)) {
                            favs.remove(wadokuId);
                        }
                        editor.putStringSet("favorites", favs);

                        editor.apply();

                        layout.removeView(tvFurigana);
                        layout.removeView(tvKanji);
                        layout.removeView(wvGerman);
                        layout.removeView(v);

                        Toast.makeText(getApplicationContext(), "Eintrag aus Favoriten entfernt.", Toast.LENGTH_LONG).show();
                    }
                });
                layout.addView(btnRemove);
            }
            if(i % 2 == 0) {
                tvFurigana.setBackgroundResource(R.color.bg_entry1);
                tvKanji.setBackgroundResource(R.color.bg_entry1);
                wvGerman.setBackgroundColor(ContextCompat.getColor(this, R.color.bg_entry1));
                if(btnRemove != null) {
                    btnRemove.setBackgroundResource(R.color.bg_entry1);
                }
            } else {
                tvFurigana.setBackgroundResource(R.color.bg_entry2);
                tvKanji.setBackgroundResource(R.color.bg_entry2);
                wvGerman.setBackgroundColor(ContextCompat.getColor(this, R.color.bg_entry2));
                if(btnRemove != null) {
                    btnRemove.setBackgroundResource(R.color.bg_entry2);
                }
            }
            i++;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.


        //getMenuInflater().inflate(R.menu.menu_favorites, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /** start search */
    public void searchInFavorites(View v) {
        List<String> findings = new ArrayList<>();
        EditText edtSearch = findViewById(R.id.edtSearch);
        try {
            final String search = URLEncoder.encode(edtSearch.getText().toString(), "UTF-8");
            Set<String> allFavs = settings.getStringSet("favorites", new HashSet<String>());
            for(String id: allFavs) {
                if(settings.getString("furigana_" + id, "").toLowerCase().contains(search.toLowerCase())) {
                    findings.add(id);
                } else if(settings.getString("kanji_" + id, "").toLowerCase().contains(search.toLowerCase())) {
                    findings.add(id);
                // TODO exclude html
                } else if(settings.getString("german_html_" + id, "").toLowerCase().contains(search.toLowerCase())) {
                    findings.add(id);
                }
            }
        } catch (UnsupportedEncodingException e) {
            //e.printStackTrace();
        }

        if(findings.isEmpty()) {
            Toast.makeText(getApplicationContext(), "Keine Einträge gefunden!", Toast.LENGTH_LONG).show();
        } else {
            layout.removeAllViews();
            int i = 0;
            for (String id : findings) {
                final TextView tvFurigana = new TextView(this);
                final String furigana = settings.getString("furigana_" + id, "");
                tvFurigana.setText(furigana);
                tvFurigana.setTextSize(MainActivity.FURIGANA_SIZE);
                layout.addView(tvFurigana);

                final TextView tvKanji = new TextView(this);
                final String kanji = settings.getString("kanji_" + id, "");
                tvKanji.setText(kanji);
                tvKanji.setTextSize(MainActivity.KANJI_SIZE);
                tvKanji.setTypeface(tvKanji.getTypeface(), Typeface.BOLD);
                layout.addView(tvKanji);

                final WebView wvGerman = new WebView(this);
                String css = MainActivity.readRawTextFile(this, R.raw.style);
                String htmlData = settings.getString("german_html_" + id, "");
                wvGerman.loadDataWithBaseURL("", htmlData, "text/html", "UTF-8", null);
                layout.addView(wvGerman);

                final String wadokuId = id;
                btnRemove = null;
                if(!settings.contains("entry_" + wadokuId)) {
                    btnRemove = new Button(this);
                    btnRemove.setText("entfernen");
                    btnRemove.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            SharedPreferences.Editor editor = settings.edit();
                            editor.remove("furigana_" + wadokuId);
                            editor.remove("kanji_" + wadokuId);
                            editor.remove("german_html_" + wadokuId);

                            Set<String> favs = settings.getStringSet("favorites", new HashSet<String>());
                            if (favs.contains(wadokuId)) {
                                favs.remove(wadokuId);
                            }
                            editor.putStringSet("favorites", favs);

                            editor.apply();

                            layout.removeView(tvFurigana);
                            layout.removeView(tvKanji);
                            layout.removeView(wvGerman);
                            layout.removeView(v);
                        }
                    });
                    layout.addView(btnRemove);
                }
                if(i % 2 == 0) {
                    tvFurigana.setBackgroundResource(R.color.bg_entry1);
                    tvKanji.setBackgroundResource(R.color.bg_entry1);
                    wvGerman.setBackgroundColor(ContextCompat.getColor(this, R.color.bg_entry1));
                    if(btnRemove != null) {
                        btnRemove.setBackgroundResource(R.color.bg_entry1);
                    }
                } else {
                    tvFurigana.setBackgroundResource(R.color.bg_entry2);
                    tvKanji.setBackgroundResource(R.color.bg_entry2);
                    wvGerman.setBackgroundColor(ContextCompat.getColor(this, R.color.bg_entry2));
                    if(btnRemove != null) {
                        btnRemove.setBackgroundResource(R.color.bg_entry2);
                    }
                }
                i++;
            }
        }
    }
}
